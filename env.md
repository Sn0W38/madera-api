SALT_ROUNDS=10
DB_USER=turnstyle
DB_PASSWORD=motdepasse
DB_HOST=localhost
DB_NAME=madera-prisma
FRONT_HOST=https://madera.kvly.fr
PORT=3306

DATABASE_URL=mysql://${DB_USER}:${DB_PASSWORD}@${DB_HOST}:${PORT}/${DB_NAME}
JWT_SECRET=In0rKljpHXUa1ulOGUoG
